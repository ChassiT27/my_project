#include <stdio.h>
#include "dice.h"

int main()
{
	int n, f;
	printf("Digite o numero de faces que deseja para o dado: \n");
	scanf("%d", &n);
	
	initializeSeed();
	f = (rollDice() % n) + 1;
	printf("Let's roll time dice: %d\n", f);
	return 0;	
}

